export default {
  characteristics: [
    {
      properties: {
        Read: 'Read',
      },
      characteristic: '2a00',
      service: '1800',
    },
    {
      properties: {
        Read: 'Read',
      },
      characteristic: '2a01',
      service: '1800',
    },
    {
      properties: {
        Read: 'Read',
      },
      characteristic: '2a04',
      service: '1800',
    },
    {
      descriptors: [
        {
          value: null,
          uuid: '2902',
        },
      ],
      properties: {
        Indicate: 'Indicate',
        Read: 'Read',
      },
      characteristic: '2a05',
      service: '1801',
    },
    {
      properties: {
        Read: 'Read',
      },
      characteristic: '2a25',
      service: '180a',
    },
    {
      properties: {
        Read: 'Read',
      },
      characteristic: '2a27',
      service: '180a',
    },
    {
      properties: {
        Read: 'Read',
      },
      characteristic: '2a28',
      service: '180a',
    },
    {
      properties: {
        Read: 'Read',
      },
      characteristic: '2a23',
      service: '180a',
    },
    {
      properties: {
        Read: 'Read',
      },
      characteristic: '2a50',
      service: '180a',
    },
    {
      descriptors: [
        {
          value: null,
          uuid: '2902',
        },
      ],
      properties: {
        Notify: 'Notify',
        Write: 'Write',
      },
      characteristic: '00001531-0000-3512-2118-0009af100700',
      service: '00001530-0000-3512-2118-0009af100700',
    },
    {
      properties: {
        WriteWithoutResponse: 'WriteWithoutResponse',
      },
      characteristic: '00001532-0000-3512-2118-0009af100700',
      service: '00001530-0000-3512-2118-0009af100700',
    },
    {
      descriptors: [
        {
          value: null,
          uuid: '2901',
        },
      ],
      properties: {
        Write: 'Write',
      },
      characteristic: '2a46',
      service: '1811',
    },
    {
      descriptors: [
        {
          value: null,
          uuid: '2902',
        },
      ],
      properties: {
        Notify: 'Notify',
        Write: 'Write',
        Read: 'Read',
      },
      characteristic: '2a44',
      service: '1811',
    },
    {
      properties: {
        WriteWithoutResponse: 'WriteWithoutResponse',
      },
      characteristic: '2a06',
      service: '1802',
    },
    {
      descriptors: [
        {
          value: null,
          uuid: '2902',
        },
      ],
      properties: {
        Notify: 'Notify',
      },
      characteristic: '2a37',
      service: '180d',
    },
    {
      properties: {
        Write: 'Write',
        Read: 'Read',
      },
      characteristic: '2a39',
      service: '180d',
    },
    {
      descriptors: [
        {
          value: null,
          uuid: '2902',
        },
      ],
      properties: {
        Notify: 'Notify',
        Write: 'Write',
        Read: 'Read',
      },
      characteristic: '2a2b',
      service: 'fee0',
    },
    {
      descriptors: [
        {
          value: null,
          uuid: '2902',
        },
      ],
      properties: {
        Notify: 'Notify',
        WriteWithoutResponse: 'WriteWithoutResponse',
      },
      characteristic: '00000001-0000-3512-2118-0009af100700',
      service: 'fee0',
    },
    {
      descriptors: [
        {
          value: null,
          uuid: '2902',
        },
      ],
      properties: {
        Notify: 'Notify',
      },
      characteristic: '00000002-0000-3512-2118-0009af100700',
      service: 'fee0',
    },
    {
      descriptors: [
        {
          value: null,
          uuid: '2902',
        },
      ],
      properties: {
        Notify: 'Notify',
        WriteWithoutResponse: 'WriteWithoutResponse',
      },
      characteristic: '00000003-0000-3512-2118-0009af100700',
      service: 'fee0',
    },
    {
      descriptors: [
        {
          value: null,
          uuid: '2902',
        },
      ],
      properties: {
        Notify: 'Notify',
        WriteWithoutResponse: 'WriteWithoutResponse',
        Read: 'Read',
      },
      characteristic: '2a04',
      service: 'fee0',
    },
    {
      descriptors: [
        {
          value: null,
          uuid: '2902',
        },
      ],
      properties: {
        Notify: 'Notify',
        WriteWithoutResponse: 'WriteWithoutResponse',
      },
      characteristic: '00000004-0000-3512-2118-0009af100700',
      service: 'fee0',
    },
    {
      descriptors: [
        {
          value: null,
          uuid: '2902',
        },
      ],
      properties: {
        Notify: 'Notify',
      },
      characteristic: '00000005-0000-3512-2118-0009af100700',
      service: 'fee0',
    },
    {
      descriptors: [
        {
          value: null,
          uuid: '2902',
        },
      ],
      properties: {
        Notify: 'Notify',
        Read: 'Read',
      },
      characteristic: '00000006-0000-3512-2118-0009af100700',
      service: 'fee0',
    },
    {
      descriptors: [
        {
          value: null,
          uuid: '2902',
        },
      ],
      properties: {
        Notify: 'Notify',
        Read: 'Read',
      },
      characteristic: '00000007-0000-3512-2118-0009af100700',
      service: 'fee0',
    },
    {
      descriptors: [
        {
          value: null,
          uuid: '2902',
        },
      ],
      properties: {
        Notify: 'Notify',
        Write: 'Write',
      },
      characteristic: '00000008-0000-3512-2118-0009af100700',
      service: 'fee0',
    },
    {
      descriptors: [
        {
          value: null,
          uuid: '2902',
        },
      ],
      properties: {
        Notify: 'Notify',
      },
      characteristic: '00000010-0000-3512-2118-0009af100700',
      service: 'fee0',
    },
    {
      descriptors: [
        {
          value: null,
          uuid: '2902',
        },
      ],
      properties: {
        Notify: 'Notify',
        WriteWithoutResponse: 'WriteWithoutResponse',
        Read: 'Read',
      },
      characteristic: '00000020-0000-3512-2118-0009af100700',
      service: 'fee0',
    },
    {
      descriptors: [
        {
          value: null,
          uuid: '2901',
        },
      ],
      properties: {
        Write: 'Write',
      },
      characteristic: '0000000e-0000-3512-2118-0009af100700',
      service: 'fee0',
    },
    {
      descriptors: [
        {
          value: null,
          uuid: '2902',
        },
      ],
      properties: {
        Notify: 'Notify',
        WriteWithoutResponse: 'WriteWithoutResponse',
      },
      characteristic: '0000000f-0000-3512-2118-0009af100700',
      service: 'fee0',
    },
    {
      descriptors: [
        {
          value: null,
          uuid: '2902',
        },
      ],
      properties: {
        Notify: 'Notify',
        WriteWithoutResponse: 'WriteWithoutResponse',
        Read: 'Read',
      },
      characteristic: '00000009-0000-3512-2118-0009af100700',
      service: 'fee1',
    },
    {
      properties: {
        Write: 'Write',
      },
      characteristic: 'fedd',
      service: 'fee1',
    },
    {
      properties: {
        Read: 'Read',
      },
      characteristic: 'fede',
      service: 'fee1',
    },
    {
      properties: {
        Read: 'Read',
      },
      characteristic: 'fedf',
      service: 'fee1',
    },
    {
      properties: {
        Write: 'Write',
        Read: 'Read',
      },
      characteristic: 'fed0',
      service: 'fee1',
    },
    {
      properties: {
        Write: 'Write',
        Read: 'Read',
      },
      characteristic: 'fed1',
      service: 'fee1',
    },
    {
      properties: {
        Read: 'Read',
      },
      characteristic: 'fed2',
      service: 'fee1',
    },
    {
      properties: {
        Write: 'Write',
        Read: 'Read',
      },
      characteristic: 'fed3',
      service: 'fee1',
    },
    {
      descriptors: [
        {
          value: null,
          uuid: '2902',
        },
      ],
      properties: {
        Notify: 'Notify',
        Write: 'Write',
        Read: 'Read',
      },
      characteristic: '0000fec1-0000-3512-2118-0009af100700',
      service: 'fee1',
    },
  ],
  services: [
    {
      uuid: '1800',
    },
    {
      uuid: '1801',
    },
    {
      uuid: '180a',
    },
    {
      uuid: '00001530-0000-3512-2118-0009af100700',
    },
    {
      uuid: '1811',
    },
    {
      uuid: '1802',
    },
    {
      uuid: '180d',
    },
    {
      uuid: 'fee0',
    },
    {
      uuid: 'fee1',
    },
  ],
  advertising: {
    txPowerLevel: -2147483648,
    serviceData: {
      fee0: {
        bytes: [0, 0, 0, 0],
        data: 'AAAAAA==',
        CDVType: 'ArrayBuffer',
      },
    },
    serviceUUIDs: ['fee0'],
    localName: 'Mi Band 3',
    isConnectable: true,
    manufacturerData: {
      bytes: [
        2,
        1,
        6,
        27,
        255,
        87,
        1,
        0,
        8,
        26,
        73,
        232,
        28,
        41,
        142,
        201,
        94,
        230,
        114,
        180,
        112,
        84,
        48,
        167,
        3,
        215,
        129,
        242,
        234,
        214,
        186,
        10,
        9,
        77,
        105,
        32,
        66,
        97,
        110,
        100,
        32,
        51,
        3,
        2,
        224,
        254,
        7,
        22,
        224,
        254,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
      ],
      data:
        'AgEGG/9XAQAIGknoHCmOyV7mcrRwVDCnA9eB8urWugoJTWkgQmFuZCAzAwLg/gcW4P4AAAAAAAAAAAAAAAA=',
      CDVType: 'ArrayBuffer',
    },
  },
  rssi: -56,
  id: 'D7:81:F2:EA:D6:BA',
  name: 'Mi Band 3',
};

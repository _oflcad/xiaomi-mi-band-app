import React, {Component} from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  NativeAppEventEmitter,
  NativeEventEmitter,
  NativeModules,
  Platform,
  PermissionsAndroid,
  ScrollView,
  AppState,
  FlatList,
  Dimensions,
} from 'react-native';
import BleManager from 'react-native-ble-manager';
import Buffer from 'buffer';
import {stringToBytes} from 'convert-string';
const window = Dimensions.get('window');
const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);
const UUID_BASE = x => `0000${x}-0000-3512-2118-0009af100700`;

function parseDate(buff) {
  let year = buff.readUInt16LE(0),
    mon = buff[2] - 1,
    day = buff[3],
    hrs = buff[4],
    min = buff[5],
    sec = buff[6],
    msec = (buff[8] * 1000) / 256;
  var today = new Date(year, mon, day, hrs, min, sec);
  return today.toLocaleTimeString();
}

export default class App extends Component {
  constructor() {
    super();

    this.state = {
      scanning: false,
      peripherals: new Map(),
      appState: '',
    };

    this.handleDiscoverPeripheral = this.handleDiscoverPeripheral.bind(this);
    this.handleStopScan = this.handleStopScan.bind(this);
    this.handleUpdateValueForCharacteristic = this.handleUpdateValueForCharacteristic.bind(
      this,
    );
    this.handleDisconnectedPeripheral = this.handleDisconnectedPeripheral.bind(
      this,
    );
    this.handleAppStateChange = this.handleAppStateChange.bind(this);
  }

  componentDidMount() {
    AppState.addEventListener('change', this.handleAppStateChange);

    BleManager.start({showAlert: false});

    this.handlerDiscover = bleManagerEmitter.addListener(
      'BleManagerDiscoverPeripheral',
      this.handleDiscoverPeripheral,
    );
    this.handlerStop = bleManagerEmitter.addListener(
      'BleManagerStopScan',
      this.handleStopScan,
    );
    this.handlerDisconnect = bleManagerEmitter.addListener(
      'BleManagerDisconnectPeripheral',
      this.handleDisconnectedPeripheral,
    );
    this.handlerUpdate = bleManagerEmitter.addListener(
      'BleManagerDidUpdateValueForCharacteristic',
      this.handleUpdateValueForCharacteristic,
    );

    if (Platform.OS === 'android' && Platform.Version >= 23) {
      PermissionsAndroid.check(
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
      ).then(result => {
        if (result) {
          console.log('Permission is OK');
        } else {
          PermissionsAndroid.requestPermission(
            PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
          ).then(result => {
            if (result) {
              console.log('User accept');
            } else {
              console.log('User refuse');
            }
          });
        }
      });
    }
  }

  handleAppStateChange(nextAppState) {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      console.log('App has come to the foreground!');
      BleManager.getConnectedPeripherals([]).then(peripheralsArray => {
        console.log('Connected peripherals: ' + peripheralsArray.length);
      });
    }
    this.setState({appState: nextAppState});
  }

  componentWillUnmount() {
    this.handlerDiscover.remove();
    this.handlerStop.remove();
    this.handlerDisconnect.remove();
    this.handlerUpdate.remove();
  }

  handleDisconnectedPeripheral(data) {
    let peripherals = this.state.peripherals;
    let peripheral = peripherals.get(data.peripheral);
    if (peripheral) {
      peripheral.connected = false;
      peripherals.set(peripheral.id, peripheral);
      this.setState({peripherals});
    }
    console.log('Disconnected from ' + data.peripheral);
  }

  handleUpdateValueForCharacteristic(data) {
    //let dataBuffer = Buffer.Buffer.from(data);
    console.log(
      'Received data from ' +
        data.peripheral +
        ' characteristic ' +
        data.characteristic,
      data.value,
    );

    let res = Buffer.Buffer.from(data.value);
    console.log('Heart rate : ' + res.readUInt16BE(0));
    /*console.log('Read !! Steps Count:', res.readUInt16LE(1));
    if (res.length >= 8) {
      console.log('Read !! Distance:', res.readUInt32LE(5));
    }
    if (res.length >= 12) {
      console.log('Read !! Calories:', res.readUInt32LE(9));
    } */
  }

  handleStopScan() {
    console.log('Scan is stopped');
    this.setState({scanning: false});
  }

  startScan() {
    if (!this.state.scanning) {
      //this.setState({peripherals: new Map()});
      BleManager.scan([], 3, true).then(results => {
        console.log('Scanning...');
        this.setState({scanning: true});
      });
    }
  }

  retrieveConnected() {
    BleManager.getConnectedPeripherals([]).then(results => {
      if (results.length === 0) {
        console.log('No connected peripherals');
      }
      console.log(results);
      var peripherals = this.state.peripherals;
      for (var i = 0; i < results.length; i++) {
        var peripheral = results[i];
        peripheral.connected = true;
        peripherals.set(peripheral.id, peripheral);
        this.setState({peripherals});
      }
    });
  }

  handleDiscoverPeripheral(peripheral) {
    var peripherals = this.state.peripherals;
    //sconsole.log('Got ble peripheral', peripheral);
    /* if (!peripheral.name) {
      peripheral.name = 'NO NAME';
    } */
    peripherals.set(peripheral.id, peripheral);
    this.setState({peripherals});
  }

  test(peripheral) {
    if (peripheral) {
      if (peripheral.connected) {
        BleManager.disconnect(peripheral.id);
      } else {
        BleManager.connect(peripheral.id)
          .then(() => {
            let peripherals = this.state.peripherals;
            let p = peripherals.get(peripheral.id);
            if (p) {
              p.connected = true;
              peripherals.set(peripheral.id, p);
              this.setState({peripherals});
            }
            console.log('Connected to ' + peripheral.id);

            console.log('Starting to retreive all services');
            BleManager.retrieveServices(peripheral.id).then(() => {
              BleManager.readRSSI(peripheral.id).then(rssi => {
                console.log('Retrieved actual RSSI value', rssi);
              });
            });
            BleManager.createBond(peripheral.id)
              .then(() => {
                console.log(
                  'createBond success or there is already an existing one',
                );
              })
              .catch(() => {
                console.log('fail to bond');
              });

            this.handleBattery(peripheral);
            this.handleDeviceInfo(peripheral);
            this.authenticateDevice(peripheral);
            this.showNotification(peripheral);
            // 
            // this.prepareHeartRate(peripheral);
            // this.prepareUserInfo(peripheral);
            // this.handleHeartRate(peripheral);
            //this.handlePedometerStats(peripheral);
          })
          .catch(error => {
            console.log('Connection error', error);
          });
      }
    }
  }

  showNotification(peripheral) {
    console.log('Device' + peripheral.name + 'Service: 1802 && Char: 2a06');
    setTimeout(() => {
      BleManager.retrieveServices(peripheral.id)
        .then(() => {
          BleManager.write(peripheral.id, '1802', '2a06', [0x03])
            .then(() => {
              console.log('SUCCESS WRITE !! CHECK BAND FOR MESSAGE');
            })
            .catch(error => {
              console.log('ERROR WRITE !! CHECK BAND FOR MESSAGE', error);
            });
        })
        .catch(error => {
          console.log('Error Retrieving services', error);
        });
    }, 6000);
  }

  authenticateDevice(peripheral) {
    let data = new Buffer.Buffer(16);
    setTimeout(() => {
      BleManager.retrieveServices(peripheral.id)
        .then(() => {
          BleManager.startNotification(peripheral.id, 'fee1', UUID_BASE('0009'))
            .then(() => {
              console.log('Notification started for Services Auth ');
              BleManager.writeWithoutResponse(
                peripheral.id,
                'fee1',
                UUID_BASE('0009'),
                [0x02],
              )
                .then(() => {
                  console.log('SUCESS WRITE !! AUTHENTICATED');
                })
                .catch(error => {
                  console.log('ERROR WRITE !! AUTHENTICED:', error);
                });
            })
            .catch(error => {
              console.log('ERROR !! auth', error);
            });
        })
        .catch(() => {
          console.log('retreive error');
        });
    }, 5000);
  }

  prepareUserInfo(peripheral) {
    var data = new ArrayBuffer(16);
    data.writeUInt8(0x4f, 0); // Set user info command
    data.writeUInt16LE(160, 8); // cm
    data.writeUInt16LE(90, 10); // kg
    data.writeUInt32LE(1, 12); // user ID
    console.log('our data has been written');
    setTimeout(() => {
      BleManager.retrieveServices(peripheral.id)
        .then(() => {
          BleManager.writeWithoutResponse(
            peripheral.id,
            'fee1',
            UUID_BASE('0008'),
            data,
          )
            .then(() => {
              console.log(
                'WRITE SUCCESS !! USER INFO IS WRIITEN INTO THE FUCKING BAND',
              );
            })
            .catch(error => {
              console.log(
                "WRITE ERROR !! USER INFO WASN'T WRITTEN ERROR",
                error,
              );
            });
        })
        .catch(error => {
          console.log('Error Retrieving services', error);
        });
    }, 9000);
  }

  prepareHeartRate(peripheral) {
    setTimeout(() => {
      BleManager.writeWithoutResponse(peripheral.id, '180d', '2a39', [
        0x15,
        0x01,
        0x00,
      ])
        .then(() => {
          console.log(
            'Success Write !! Send a request to HMC for continuos measurements ',
          );
        })
        .catch(error => {
          console.error(
            'Error Write !! Send a request to HMC for one-shot measurements' +
              error,
          );
        });
    }, 6000);
    setTimeout(() => {
      BleManager.writeWithoutResponse(peripheral.id, '180d', '2a39', [
        0x15,
        0x02,
        0x00,
      ])
        .then(() => {
          console.log(
            'Success Write !! Send a request to HMC  for one-shot measurements',
          );
        })
        .catch(error => {
          console.error(
            'Error Write !! Send a request to HMC for continuos measurements' +
              error,
          );
        });
    }, 7000);
    setTimeout(() => {
      BleManager.writeWithoutResponse(peripheral.id, '180d', '2a39', [
        0x15,
        0x02,
        0x01,
      ])
        .then(() => {
          console.log(
            "Success Write !! Sending command to SENS !! don't know why we need this",
          );
        })
        .catch(error => {
          console.error(
            "Error Write !!  Sending command to SENS !! don't know why we need this" +
              error,
          );
        });
    }, 8000);
  }

  handleHeartRate(peripheral) {
    setTimeout(() => {
      BleManager.retrieveServices(peripheral.id)
        .then(() => {
          BleManager.startNotification(peripheral.id, '180d', '2a37')
            .then(() => {
              console.log(
                'Notification started for Services of Heart Rate Monitor',
              );
            })
            .catch(error => {
              console.log('Error Read HRM', error);
            });
        })
        .catch(error => {
          console.log('Error Retrieving services', error);
        });
    }, 10000);
  }

  handleBattery(peripheral) {
    setTimeout(() => {
      BleManager.retrieveServices(peripheral.id).then(services => {
        BleManager.read(peripheral.id, 'fee0', UUID_BASE('0006'))
          .then(readData => {
            let data = Buffer.Buffer.from(readData);
            console.log('read battery level', data.readUInt8(1, true));
          })
          .catch(error => {
            console.log('error', error);
          });
      });
    }, 1000);
  }

  handleDeviceInfo(peripheral) {
    setTimeout(() => {
      BleManager.retrieveServices(peripheral.id).then(services => {
        BleManager.read(peripheral.id, '180a', '2a28')
          .then(readData => {
            let data = Buffer.Buffer.from(readData);
            console.log('Read !! Firmware Version : ', data.toString());
          })
          .catch(error => {
            console.log('error', error);
          });
      });
    }, 2000);
    setTimeout(() => {
      BleManager.retrieveServices(peripheral.id).then(services => {
        BleManager.read(peripheral.id, '180a', '2a25')
          .then(readData => {
            let data = Buffer.Buffer.from(readData);
            console.log('Read !! Serial Number :', data.toString());
          })
          .catch(error => {
            console.log('error', error);
          });
      });
    }, 3000);
    setTimeout(() => {
      BleManager.retrieveServices(peripheral.id).then(services => {
        BleManager.read(peripheral.id, 'fee0', '2a2b')
          .then(readData => {
            let data = Buffer.Buffer.from(readData);
            console.log('Read !! Time info :', parseDate(data).toString());
          })
          .catch(error => {
            console.log('error', error);
          });
      });
    }, 4000);
  }

  handlePedometerStats(peripheral) {
    setTimeout(() => {
      BleManager.retrieveServices(peripheral.id)
        .then(() => {
          BleManager.startNotification(peripheral.id, 'fee0', UUID_BASE('0007'))
            .then(() => {
              console.log(
                'Notification started for Services of Pedometer stats',
              );
            })
            .catch(error => {
              console.log('Read Steps Error', error);
            });
        })
        .catch(error => {
          console.log('Error ', error);
        });
    }, 6000);
  }

  renderItem(item) {
    const color = item.connected ? 'green' : '#fff';
    return (
      <TouchableHighlight onPress={() => this.test(item)}>
        <View style={[styles.row, {backgroundColor: color}]}>
          <Text
            style={{
              fontSize: 12,
              textAlign: 'center',
              color: '#333333',
              padding: 10,
            }}>
            {item.name}
          </Text>
          <Text
            style={{
              fontSize: 10,
              textAlign: 'center',
              color: '#333333',
              padding: 2,
            }}>
            RSSI: {item.rssi}
          </Text>
          <Text
            style={{
              fontSize: 8,
              textAlign: 'center',
              color: '#333333',
              padding: 2,
              paddingBottom: 20,
            }}>
            {item.id}
          </Text>
        </View>
      </TouchableHighlight>
    );
  }

  render() {
    const list = Array.from(this.state.peripherals.values());
    list.sort(el => !el.name);
    return (
      <View style={styles.container}>
        <TouchableHighlight
          style={{
            marginTop: 40,
            margin: 20,
            padding: 20,
            backgroundColor: '#ccc',
          }}
          onPress={() => this.startScan()}>
          <Text>Scan Bluetooth ({this.state.scanning ? 'on' : 'off'})</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={{
            marginTop: 0,
            margin: 20,
            padding: 20,
            backgroundColor: '#ccc',
          }}
          onPress={() => this.retrieveConnected()}>
          <Text>Retrieve connected peripherals</Text>
        </TouchableHighlight>
        <ScrollView style={styles.scroll}>
          {list.length == 0 && (
            <View style={{flex: 1, margin: 20}}>
              <Text style={{textAlign: 'center'}}>No peripherals</Text>
            </View>
          )}
          <FlatList
            data={list}
            renderItem={({item}) => this.renderItem(item)}
            keyExtractor={item => item.id}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF',
    width: window.width,
    height: window.height,
  },
  scroll: {
    flex: 1,
    backgroundColor: '#f0f0f0',
    margin: 10,
  },
  row: {
    margin: 10,
  },
});
